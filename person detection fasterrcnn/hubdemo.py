# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier uses it to perform object detection on a video.
# It draws boxes and scores around the objects of interest in each frame
# of the video.



# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import argparse

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util


parser = argparse.ArgumentParser(description='person detection Arguments')
parser.add_argument("-m", "--model", dest="MODEL_NAME", help="file path to config file")
parser.add_argument("-v", "--video", dest="VIDEO_NAME", help="video file path ")
parser.add_argument("-o", "--videooutput",dest = "OUTPUT_VIDEOFILENAME",help="video file output")


args = parser.parse_args()


MODEL_NAME = args.MODEL_NAME
VIDEO_NAME = args.VIDEO_NAME
OUTPUT_VIDEOFILENAME =args.OUTPUT_VIDEOFILENAME
# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
CWD_PATH = os.getcwd()

PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'training','labelmap.pbtxt')

# Path to video
PATH_TO_VIDEO = os.path.join(CWD_PATH,VIDEO_NAME)

# Number of classes the object detector can identify
NUM_CLASSES = 1

# Load the label map.
# Label maps map indices to category names, so that when our convolution
# network predicts `1`, we know that this corresponds to `person`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Open video file
def save_webcam(outPath,fps,mirror=False):
    # inserting video from disk:
    video = cv2.VideoCapture(PATH_TO_VIDEO)
 
    currentFrame = 0
 
    # Get current width of frame
    width = video.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
    # Get current height of frame
    height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
 
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    out = cv2.VideoWriter(outPath, fourcc, fps, (int(width), int(height)))




    while(video.isOpened()):

    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
        ret, frame = video.read()
        frame_expanded = np.expand_dims(frame, axis=0)

    # Perform the actual detection by running the model with the image as input
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: frame_expanded})

    # Draw the results of the detection (aka 'visulaize the results')
        vis_util.visualize_boxes_and_labels_on_image_array(
            frame,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=8,
            min_score_thresh=0.60)
        final_score = np.squeeze(scores)    
        count = 0
        for i in range(100):
    	       if scores is None or final_score[i] > 0.5:
        	       count = count + 1
        txt = "person Count = {}".format(count)
        cv2.putText(frame, txt, (50,50), cv2.FONT_HERSHEY_SIMPLEX, .8, (0,0,255), 2)


    # All the results have been drawn on the frame, so it's time to display it.
        cv2.imshow('Object detector', frame)

        if ret == True:
            if mirror == True:
                # Mirror the output video frame
                #frame = cv2.flip(frame, 1)
            # Saves for video
                out.write(frame)
 
            # Display the resulting frame
            cv2.imshow('frame', frame)
        else:
            break

    # Press 'q' to quit
        if cv2.waitKey(1) == ord('q'):
            break
        currentFrame += 1
 
    #out = cv2.VideoWriter('output.avi', fourcc, 1, (frame_height, frame_width)) 
        

# Clean up
    video.release()
    video.release()
    cv2.destroyAllWindows()
 
def main():
    save_webcam(OUTPUT_VIDEOFILENAME, 30.0,mirror=True)
    
 
if __name__ == '__main__':
    main()

