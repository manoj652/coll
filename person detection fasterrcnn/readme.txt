first install dependencies then clone te repo(all the code is in (python3)


steps to run on x86 platform
----------------------------------------------------------
# clone tensorflow object detection models api

git clone https://github.com/tensorflow/models


********************************************************
cd models/research

Note:paste the below path whenever u open a terminal into models/research

export PATH=~/anaconda3/bin:$PATH   (if u use anaconda for installing dependencies)
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim 
protoc object_detection/protos/string_int_label_map.proto --python_out=.


*****************************************************************
cd models/research/objectdetection

copy files in 00personhubfiles to objectedetection folder
--------------------------------------------------------------------

for video input and video output
python hubdemo.py --model fasterrcnn1025v2 --video lr12.mp4 --videooutput ffk.avi

----------------------------------------------------------------------
for using webcam
python webcamperson.py --model f/trained/fasterrcnn1025v2 --videooutput zzz.avi 



**********************************************************
--model =folder which contains frozengraph.pb file

--video =input of the video given to the model

--videooutput = output inference generated on the video(any name we can provide example testc.avi)

training - folder contains labelmap.pbtxt
